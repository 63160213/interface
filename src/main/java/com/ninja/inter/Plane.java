/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ninja.inter;

/**
 *
 * @author USER
 */
public class Plane extends Vahicle implements Flyable,Runable {
    
    public Plane(String engine) {
        super(engine);
    }

    @Override
    public void startEngine() {
        System.out.println("Plane: startengine");
    }

    @Override
    public void stopEngine() {
        System.out.println("Plane: stopengine");
    }

    @Override
    public void raiseSpeed() {
        System.out.println("Plane: raisespeed");
    }

    @Override
    public void applyBreak() {
        System.out.println("Plane: applybreak");
    }

    @Override
    public void fly() {
        System.out.println("Plane: fly");
    }

    @Override
    public void run() {
        System.out.println("Plane: run");
    }

}
