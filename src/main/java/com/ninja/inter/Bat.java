/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ninja.inter;

/**
 *
 * @author USER
 */
public class Bat extends Poultry {

    private String nickname;

    public Bat(String nickname) {
        super();
        this.nickname = nickname;
    }

    public void eat() {
        System.out.println("Bat : " + nickname + " eat");
    }

    public void speak() {
        System.out.println("Bat : " + nickname + " speak");
    }

    public void sleep() {
        System.out.println("Bat : " + nickname + " sleep");
    }

    public void fly() {
        System.out.println("Bat : " + nickname + " fly");
    }

}
